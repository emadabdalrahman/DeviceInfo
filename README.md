# DeviceInfo

## Description

App helps you to know information about your physical device like Ram Battery and your system OS 

## Screen Shot

![](ScreenShots/imgonline-com-ua-twotoone-cG6y3usa8K4IYU.jpg)
![](ScreenShots/imgonline-com-ua-twotoone-o2c8dmWFl4lVcOqK.jpg)

## Third Party Libraries

* [Picasso](http://square.github.io/picasso/)
* [CircularFillableLoaders](https://github.com/lopspower/CircularFillableLoaders)
* [SmartTabLayout](https://github.com/ogaclejapan/SmartTabLayout)
