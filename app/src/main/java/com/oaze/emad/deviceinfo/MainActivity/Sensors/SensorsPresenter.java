package com.oaze.emad.deviceinfo.MainActivity.Sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;

import com.oaze.emad.deviceinfo.Utiles.SensorData;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.SENSOR_SERVICE;

/**
 * Created by emad on 6/9/2017.
 */

public class SensorsPresenter implements SensorsContract.Presenter {

    private Context mContext;
    private SensorsContract.View mView;

    public SensorsPresenter(SensorsContract.View View, Context context) {
        this.mView = View;
        mContext = context;
    }

    @Override
    public void Start() {
        mView.initializeRecyclerView(getSensors());
    }

    public ArrayList<SensorData> getSensors() {

        ArrayList<SensorData> sensorData = new ArrayList<>();

        SensorManager sensorManager = (SensorManager) mContext.getSystemService(SENSOR_SERVICE);
        List<Sensor> list = sensorManager.getSensorList(Sensor.TYPE_ALL);

        for (int i = 0; i < list.size(); i++) {

            list.get(i).getMaximumRange();

            SensorData data = new SensorData();
            data.setType(list.get(i).getType());
            data.setName(list.get(i).getName());
            data.setMaximumRange(list.get(i).getMaximumRange());
            data.setPower(list.get(i).getPower());
            data.setVendor(list.get(i).getVendor());
            data.setVersion(list.get(i).getVersion());
            data.setResolution(list.get(i).getResolution());
            // data.setID(list.get(i).getId());
            // data.setType(list.get(i).getStringType());
            // data.setWakeUp(list.get(i).isWakeUpSensor());
            // data.setDynamic( list.get(i).isDynamicSensor());

            sensorData.add(data);
        }
        return sensorData;
    }
}
