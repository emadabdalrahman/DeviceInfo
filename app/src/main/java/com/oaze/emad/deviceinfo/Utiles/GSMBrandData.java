package com.oaze.emad.deviceinfo.Utiles;

/**
 * Created by emad on 6/11/2017.
 */

public class GSMBrandData {
    private String Name;
    private String DeviceNum;
    private String URL;

    public GSMBrandData() {
    }

    public GSMBrandData(String name, String deviceNum) {
        Name = name;
        DeviceNum = deviceNum;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDeviceNum() {
        return DeviceNum;
    }

    public void setDeviceNum(String deviceNum) {
        DeviceNum = deviceNum;
    }
}
