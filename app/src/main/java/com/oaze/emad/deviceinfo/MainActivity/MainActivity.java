package com.oaze.emad.deviceinfo.MainActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.oaze.emad.deviceinfo.R;
import com.oaze.emad.deviceinfo.TopDevicesActivity.TopDevicesActivity;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private ViewPager mViewPager;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        initializeToolbar();
        initializeViewPager();
        initializeTabs();
        initializeDrawerLayout();
        initializeAdMod();

    }

    public void initializeAdMod() {
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    public void openTopDevicesActivity(View view) {
        Intent intent = new Intent(this, TopDevicesActivity.class);
        startActivity(intent);
    }

    public void initializeToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
    }

    public void initializeViewPager() {
        ArrayList<String> Titles = new ArrayList<>();
        Titles.add(getString(R.string.Sensors));
        Titles.add(getString(R.string.Device));
        Titles.add(getString(R.string.OS));
        Titles.add(getString(R.string.RAM));
        Titles.add(getString(R.string.Battery));

        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager(),Titles);
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mViewPager.setAdapter(viewPagerAdapter);
    }

    public void initializeTabs() {
        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(mViewPager);
    }

    public void initializeDrawerLayout() {
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
    }
}
