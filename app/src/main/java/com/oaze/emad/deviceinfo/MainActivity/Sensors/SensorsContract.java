package com.oaze.emad.deviceinfo.MainActivity.Sensors;

import com.oaze.emad.deviceinfo.Utiles.SensorData;

import java.util.ArrayList;

/**
 * Created by emad on 6/7/2017.
 */

public interface SensorsContract {

    public interface View {
        void initializeRecyclerView(ArrayList<SensorData> sensorData);
    }

    public interface Presenter {
        void Start();
    }
}
