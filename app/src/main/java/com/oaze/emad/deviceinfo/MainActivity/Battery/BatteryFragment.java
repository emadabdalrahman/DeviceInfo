package com.oaze.emad.deviceinfo.MainActivity.Battery;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oaze.emad.deviceinfo.R;
import com.oaze.emad.deviceinfo.databinding.BatteryFragmentBinding;


/**
 * Created by emad on 6/7/2017.
 */

public class BatteryFragment extends Fragment implements BatteryContract.View {

    private FragmentActivity mContext;
    private BatteryFragmentBinding mBinding;
    private BatteryPresenter mBatteryPresenter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = (FragmentActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater,R.layout.battery_fragment,container,false);

        mBatteryPresenter = new BatteryPresenter(this,mContext);
        mBatteryPresenter.Start();

        BatteryBroadcastReceiver batteryBroadcastReceiver =new BatteryBroadcastReceiver(mBinding);
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);

        mContext.registerReceiver(batteryBroadcastReceiver,intentFilter);

        return mBinding.getRoot();
    }
}
