package com.oaze.emad.deviceinfo.MainActivity.RAM;

/**
 * Created by emad on 6/6/2017.
 */

public interface RamContract {
    public interface View {

        void showTotalRam(String totalMem);

        void showFreeRam(String UsedMem);

        void showUsedRam(String FreeMem);

        void showProgressBar(int val);

        void showPercentage(String val);
    }

    public interface Presenter {
        void Start();
    }
}
