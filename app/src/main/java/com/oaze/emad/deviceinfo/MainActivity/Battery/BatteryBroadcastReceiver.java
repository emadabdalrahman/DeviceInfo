package com.oaze.emad.deviceinfo.MainActivity.Battery;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;


import com.oaze.emad.deviceinfo.R;
import com.oaze.emad.deviceinfo.databinding.BatteryFragmentBinding;


/**
 * Created by emad on 6/7/2017.
 */

public class BatteryBroadcastReceiver extends BroadcastReceiver {

    private BatteryFragmentBinding mBinding;

    public BatteryBroadcastReceiver(BatteryFragmentBinding binding) {
        this.mBinding = binding;
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        int chargePlug = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1);
        boolean usbCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_USB;
        boolean acCharge = chargePlug == BatteryManager.BATTERY_PLUGGED_AC;

        if (usbCharge) {
            mBinding.PowerSource.setText(R.string.USB);
        }
        else if (acCharge) {
            mBinding.PowerSource.setText(R.string.AC);
        }
        else if (chargePlug == BatteryManager.BATTERY_PLUGGED_WIRELESS) {
            mBinding.PowerSource.setText(R.string.WIRELESS);
        }else {

            mBinding.PowerSource.setText(R.string.Battery);
     //       mBinding.waveBatteryProgressbar.setBackgroundResource(R.drawable.ic_battery_full_black_48dp);

        }


        int health = intent.getIntExtra(BatteryManager.EXTRA_HEALTH, -1);

        if (health == BatteryManager.BATTERY_HEALTH_UNKNOWN) {
            mBinding.Health.setText(R.string.UNKNOWN);
        }
        if (health == BatteryManager.BATTERY_HEALTH_GOOD) {
            mBinding.Health.setText(R.string.GOOD);
        }
        if (health == BatteryManager.BATTERY_HEALTH_COLD) {
            mBinding.Health.setText(R.string.COLD);
        }
        if (health == BatteryManager.BATTERY_HEALTH_DEAD) {
            mBinding.Health.setText(R.string.DEAD);
        }
        if (health == BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE) {
            mBinding.Health.setText(R.string.OVER_VOLTAGE);
        }
        if (health == BatteryManager.BATTERY_HEALTH_OVERHEAT) {
            mBinding.Health.setText(R.string.OVERHEAT);
        }
        if (health == BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE) {
            mBinding.Health.setText(R.string.UNSPECIFIED_FAILURE);
        }


        int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        if (status == BatteryManager.BATTERY_STATUS_CHARGING) {
            mBinding.Status.setText(R.string.CHARGING);
        }
        if (status == BatteryManager.BATTERY_STATUS_DISCHARGING) {
            mBinding.Status.setText(R.string.DISCHARGING);
        }
        if (status == BatteryManager.BATTERY_STATUS_FULL) {
            mBinding.Status.setText(R.string.FULL);
        }
        if (status == BatteryManager.BATTERY_STATUS_NOT_CHARGING) {
            mBinding.Status.setText(R.string.NOT_CHARGING);
        }
        if (status == BatteryManager.BATTERY_STATUS_UNKNOWN) {
            mBinding.Status.setText(R.string.UNKNOWN);
        }


        mBinding.Capacity.setText("" + intent.getBooleanExtra(BatteryManager.EXTRA_PRESENT,false) + "");

        mBinding.Temperature.setText("" + intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, -1)/10.0 + " C");
        mBinding.Technology.setText("" + intent.getStringExtra(BatteryManager.EXTRA_TECHNOLOGY) + "");
        mBinding.Voltage.setText("" + intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, -1)/100.0 + " V");
        mBinding.Level.setText("" + intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) + " %");

        mBinding.waveBatteryProgressbar.setProgressValue(intent.getIntExtra(BatteryManager.EXTRA_LEVEL,-1));
        mBinding.waveBatteryProgressbar.setCenterTitle("" + intent.getIntExtra(BatteryManager.EXTRA_LEVEL,-1)+ " %");

    }
}
