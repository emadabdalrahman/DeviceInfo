package com.oaze.emad.deviceinfo.TopDevicesActivity;

import com.oaze.emad.deviceinfo.Utiles.GSMTopDevices;

import org.jsoup.nodes.Document;

import java.util.ArrayList;

/**
 * Created by emad on 6/11/2017.
 */

public interface TopDeviceContract {
    public interface View{
        void playNoInternetConnectionGIF();
        void pauseNoInternetConnectionGIF();
        void showNoInternetConnectionGIF();
        boolean isNoInternetConnectionVisible();
        void hideNoInternetConnection();
        void showNoInternetConnection();
        void initializeLoadingView();
        void startLoadingView();
        void stopLoadingView();
        void hideLoadingView();
        void showOfflineToast();
        void showSnackbar();
        void initializeRecyclerView(ArrayList<GSMTopDevices> devices);

    }
    public interface Presenter{
        void Start();
        ArrayList<GSMTopDevices> getTopDevicesLocal();
        void saveTopDevicesLocal(ArrayList<GSMTopDevices> topDevices);
        ArrayList<GSMTopDevices> getGSMTopDevices(Document document);
        void initializeLoader();
        GSMTopDevices getDevice(int position);
    }
}
