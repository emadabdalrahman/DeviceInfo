package com.oaze.emad.deviceinfo.MainActivity.RAM;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oaze.emad.deviceinfo.R;
import com.oaze.emad.deviceinfo.databinding.RamFragmentBinding;

/**
 * Created by emad on 6/6/2017.
 */

public class RamFragment extends Fragment implements RamContract.View {

    private RamFragmentBinding mBinding;
    private FragmentActivity mContext;
    private RamPresenter mRamPresenter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = (FragmentActivity)context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mBinding = DataBindingUtil.inflate(inflater,R.layout.ram_fragment,container,false);

        mRamPresenter = new RamPresenter(this, mContext);
        mRamPresenter.Start();

        return mBinding.getRoot();
    }

    @Override
    public void showTotalRam(String totalMem) {
        mBinding.totalSpaceVal.setText(String.format("%s MB", totalMem));
    }

    @Override
    public void showFreeRam(String UsedMem) {
        mBinding.freeSpaceVal.setText(String.format("%s MB",UsedMem));
    }

    @Override
    public void showUsedRam(String FreeMem) {
        mBinding.usedSpaceVal.setText(String.format("%s MB",FreeMem));
    }

    @Override
    public void showProgressBar(int val) {
        mBinding.ramCircularFillableLoaders.setProgress(val);
    }

    @Override
    public void showPercentage(String val) {
        mBinding.usedPercentage.setText(String.format("%s %%", val));
    }

}
