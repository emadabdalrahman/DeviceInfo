package com.oaze.emad.deviceinfo.TopDevicesActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cunoraz.gifview.library.GifView;
import com.ldoublem.loadingviewlib.view.LVGhost;
import com.oaze.emad.deviceinfo.DeviceDetailsActivity.DeviceDetailsActivity;
import com.oaze.emad.deviceinfo.R;
import com.oaze.emad.deviceinfo.Utiles.GSMTopDevices;

import java.util.ArrayList;


/**
 * Created by emad on 6/9/2017.
 */

public class TopDevicesFragment extends Fragment implements TopDevicesRecyclerViewAdepter.ClickListener, TopDeviceContract.View {

    private View mRootView;
    private FragmentActivity mContext;
    private TopDevicesPresenter mTopDevicesPresenter;
    private RelativeLayout mNoInternetConnectionLayout;
    private GifView mGifView;
    private LVGhost mLvGhostLoadingView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = (FragmentActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.top_devices_fragment, container, false);
        mNoInternetConnectionLayout = (RelativeLayout) mRootView.findViewById(R.id.top_devices_offline_layout);

        mTopDevicesPresenter = new TopDevicesPresenter(mContext, this);
        mTopDevicesPresenter.Start();

        return mRootView;
    }

    @Override
    public void itemOnClickListener(int position) {

        Intent intent = new Intent(mContext, DeviceDetailsActivity.class);
        intent.putExtra("URL", mTopDevicesPresenter.getDevice(position).getURL());
        intent.putExtra("Name", mTopDevicesPresenter.getDevice(position).getName());
        startActivity(intent);

    }

    @Override
    public void playNoInternetConnectionGIF() {
        mGifView.play();
    }

    @Override
    public void pauseNoInternetConnectionGIF() {
        mGifView.pause();
    }

    @Override
    public void showNoInternetConnectionGIF() {
        mGifView = (GifView) mRootView.findViewById(R.id.gif1);
        mGifView.setVisibility(View.VISIBLE);
        mGifView.setGifResource(R.drawable.no_internet_connection);
    }

    @Override
    public boolean isNoInternetConnectionVisible() {
        return mNoInternetConnectionLayout.isShown();
    }

    @Override
    public void hideNoInternetConnection() {
        if (isNoInternetConnectionVisible()) {
            mNoInternetConnectionLayout.setVisibility(View.INVISIBLE);
            mRootView.setBackgroundResource(R.color.colorPrimary);

            showNoInternetConnection();
        }
    }

    @Override
    public void showNoInternetConnection() {
        if (!isNoInternetConnectionVisible()) {
            mNoInternetConnectionLayout.setVisibility(View.VISIBLE);
          //  mRootView.setBackgroundColor(Color.WHITE);
            mRootView.setBackgroundResource(R.color.white2);
        }
    }

    @Override
    public void initializeLoadingView() {
        mLvGhostLoadingView = (LVGhost) mRootView.findViewById(R.id.top_device_fragment_loadingView);
        mLvGhostLoadingView.setViewColor(Color.WHITE);
        mLvGhostLoadingView.setHandColor(Color.BLACK);
    }

    @Override
    public void startLoadingView() {
        mLvGhostLoadingView.startAnim();
    }

    @Override
    public void stopLoadingView() {
        mLvGhostLoadingView.stopAnim();
    }

    @Override
    public void hideLoadingView() {
        mLvGhostLoadingView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showOfflineToast() {
        Toast.makeText(mContext,R.string.Offline_Mode,Toast.LENGTH_LONG).show();
    }

    @Override
    public void showSnackbar() {
        Snackbar.make(mRootView,R.string.Offline_Mode,Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void initializeRecyclerView(ArrayList<GSMTopDevices> Devices) {
        RecyclerView recyclerView = (RecyclerView) mRootView.findViewById(R.id.top_device_fragment_recyclerView);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager manager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(manager);

        TopDevicesRecyclerViewAdepter adepter = new TopDevicesRecyclerViewAdepter(Devices, mContext, this);
        recyclerView.setAdapter(adepter);

    }

}
