package com.oaze.emad.deviceinfo.Model.Remote;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.oaze.emad.deviceinfo.R;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by emad on 6/9/2017.
 */

public class DeviceDetailsAsyncLoader extends AsyncTaskLoader<Document> {

    private String mURL;
    private Context mContext;

    public DeviceDetailsAsyncLoader(Context context, String URL) {
        super(context);
        mURL = URL;
        mContext = context;
    }

    @Override
    public Document loadInBackground() {

        try {
            Connection.Response response = Jsoup.connect(mURL)
                    .timeout(10 * 1000)
                    .userAgent(mContext.getString(R.string.UserAgent))
                    .execute();
            return response.parse();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
