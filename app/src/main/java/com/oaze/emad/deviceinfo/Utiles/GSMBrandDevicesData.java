package com.oaze.emad.deviceinfo.Utiles;

/**
 * Created by emad on 6/11/2017.
 */

public class GSMBrandDevicesData {
    private String Name;
    private String Description;
    private String URL;

    public GSMBrandDevicesData(String name, String description, String URL) {
        Name = name;
        Description = description;
        this.URL = URL;
    }

    public GSMBrandDevicesData() {
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }
}
