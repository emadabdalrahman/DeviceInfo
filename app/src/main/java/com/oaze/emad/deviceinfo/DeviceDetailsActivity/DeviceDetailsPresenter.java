package com.oaze.emad.deviceinfo.DeviceDetailsActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.oaze.emad.deviceinfo.Model.Local.DbContract;
import com.oaze.emad.deviceinfo.Model.Remote.DeviceDetailsAsyncLoader;
import com.oaze.emad.deviceinfo.Utiles.GSMDeviceData;
import com.oaze.emad.deviceinfo.Utiles.Network;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by emad on 6/27/2017.
 */

public class DeviceDetailsPresenter implements LoaderManager.LoaderCallbacks<Document>, DeviceDetailsContract.Presenter {

    private FragmentActivity mContext;
    private DeviceDetailsContract.View mView;
    private static GSMDeviceData mGSMDeviceData;

    public DeviceDetailsPresenter(FragmentActivity mContext, DeviceDetailsContract.View mView) {
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void start() {

        mView.initializeLoadingView();
        mView.startLoadingView();
        if (mGSMDeviceData == null) {

            if (Network.isNetworkAvailable(mContext)) {
                initializeLoader();
            } else {
                mGSMDeviceData = getDeviceLocal(mContext.getIntent().getStringExtra("Name"));
                if (mGSMDeviceData == null) {
                    mView.showNoInternetConnectionGIF();
                    mView.playNoInternetConnectionGIF();
                    mView.hideLoadingView();
                    mView.showNoInternetConnection();
                } else {
                    mView.hideNoInternetConnection();
                    mView.showDeviceImage(mGSMDeviceData.getImageURL());
                    mView.initializeRecyclerView(mGSMDeviceData);
                   // mView.stopLoadingView();
                    mView.hideLoadingView();
                }
            }

        } else {

            if (mGSMDeviceData.getName().equals(mContext.getIntent().getStringExtra("Name"))){
                mView.hideNoInternetConnection();
             //   mView.stopLoadingView();
                mView.hideLoadingView();
                mView.showDeviceImage(mGSMDeviceData.getImageURL());
                mView.initializeRecyclerView(mGSMDeviceData);
            }else {
                mGSMDeviceData = null;
                restart();
            }

        }

    }

    @Override
    public void restart() {
        start();
    }

    @Override
    public void initializeLoader() {
        LoaderManager loaderManager = mContext.getSupportLoaderManager();
        Loader<Document> loader = loaderManager.getLoader(100);
        if (loader == null) {
            loaderManager.initLoader(100, null, this).forceLoad();
        } else {
            loaderManager.restartLoader(100, null, this).forceLoad();
        }
    }

    @Override
    public GSMDeviceData getGSMDeviceData(Document document) {

        GSMDeviceData deviceData = new GSMDeviceData();

        deviceData.setDocument(document.html());

        Element deviceReviewHead = document.getElementsByClass("article-info").get(0);
        Element deviceReviewHeadBody = deviceReviewHead.getElementsByClass("center-stage light nobg specs-accent").get(0);

        deviceData.setName(deviceReviewHead.child(1).child(1).text());
        deviceData.setImageURL(deviceReviewHeadBody.child(0).child(0).child(0).absUrl("src"));

        Elements deviceReviewHeadBodyTable = deviceReviewHeadBody.child(1).children();

        deviceData.setReleasedDate(deviceReviewHeadBodyTable.get(0).child(0).child(1).text());
        deviceData.setWeight(deviceReviewHeadBodyTable.get(0).child(2).child(1).text());
        deviceData.setAndroidVersion(deviceReviewHeadBodyTable.get(0).child(4).child(1).text());
        deviceData.setLocalStorage(deviceReviewHeadBodyTable.get(0).child(6).child(1).text());

        deviceData.setDisplaySize(deviceReviewHeadBodyTable.get(3).child(1).text());
        deviceData.setDisplayResolution(deviceReviewHeadBodyTable.get(3).child(2).text());

        deviceData.setCameraPhoto(deviceReviewHeadBodyTable.get(4).child(1).text());
        deviceData.setCameraVideo(deviceReviewHeadBodyTable.get(4).child(2).text());


        deviceData.setRam(deviceReviewHeadBodyTable.get(5).child(1).text());
        deviceData.setChipset(deviceReviewHeadBodyTable.get(5).child(2).text());

        deviceData.setBatteryCapacity(deviceReviewHeadBodyTable.get(6).child(1).text());
        deviceData.setBatteryTechnology(deviceReviewHeadBodyTable.get(6).child(2).text());

        return deviceData;
    }

    @Override
    public boolean isSavedLocal(String deviceName) {

        Cursor cursor = mContext.getContentResolver().query(DbContract.Device.CONTENT_URI, null, null, null, null);
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                if (deviceName.equals(cursor.getString(cursor.getColumnIndex(DbContract.Device.DEVICE_NAME)))) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public boolean saveDeviceLocal(GSMDeviceData deviceData) {

        if (!isSavedLocal(deviceData.getName())) {
            ContentValues values = new ContentValues();
            values.put(DbContract.Device.DEVICE_NAME, deviceData.getName());
            values.put(DbContract.Device.DEVICE_DATA, deviceData.getDocument());
            mContext.getContentResolver().insert(DbContract.Device.CONTENT_URI, values);
        }

        return true;
    }

    @Override
    public GSMDeviceData getDeviceLocal(String deviceName) {

        GSMDeviceData deviceData = new GSMDeviceData();

        Cursor cursor = mContext.getContentResolver().query(DbContract.Device.CONTENT_URI, null, null, null, null);
        if (cursor.getCount() > 0) {
            while (cursor.moveToNext()) {
                if (cursor.getString(cursor.getColumnIndex(DbContract.Device.DEVICE_NAME)).equals(deviceName)) {
                    String html = cursor.getString(cursor.getColumnIndex(DbContract.Device.DEVICE_DATA));
                    Document document = Jsoup.parse(html);
                    deviceData = getGSMDeviceData(document);
                    return deviceData;
                }
            }
        }
        return null;
    }

    @Override
    public Loader<Document> onCreateLoader(int id, Bundle args) {
        return new DeviceDetailsAsyncLoader(mContext, mContext.getIntent().getStringExtra("URL"));
    }

    @Override
    public void onLoadFinished(Loader<Document> loader, Document data) {
        if (data != null){
            mGSMDeviceData = getGSMDeviceData(data);
            saveDeviceLocal(mGSMDeviceData);
            mView.hideNoInternetConnection();
            mView.showDeviceImage(mGSMDeviceData.getImageURL());
            mView.initializeRecyclerView(mGSMDeviceData);
           // mView.stopLoadingView();
            mView.hideLoadingView();
        }else {
            mView.showNoInternetConnectionGIF();
            mView.playNoInternetConnectionGIF();
            mView.hideLoadingView();
            mView.showNoInternetConnection();
        }
    }

    @Override
    public void onLoaderReset(Loader<Document> loader) {

    }
}
