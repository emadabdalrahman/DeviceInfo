package com.oaze.emad.deviceinfo.MainActivity.OS;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oaze.emad.deviceinfo.R;
import com.oaze.emad.deviceinfo.Utiles.SystemData;

import java.util.ArrayList;

/**
 * Created by emad on 5/31/2017.
 */

public class recyclerViewAdepter extends RecyclerView.Adapter<recyclerViewAdepter.ViewHolder> {

    private ArrayList<SystemData> mSystemData;
    private Context mContext;


    public recyclerViewAdepter(ArrayList<SystemData> systemData, Context context) {
        mSystemData = systemData;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.Name.setText(mSystemData.get(position).getName());
        holder.Data.setText(mSystemData.get(position).getData());
    }

    @Override
    public int getItemCount() {
        return mSystemData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView Data;
        TextView Name;
        public ViewHolder(View itemView) {
            super(itemView);

            Data = (TextView)itemView.findViewById(R.id.data);
            Name = (TextView)itemView.findViewById(R.id.name);
        }
    }
}
