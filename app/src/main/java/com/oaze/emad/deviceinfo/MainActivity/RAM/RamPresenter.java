package com.oaze.emad.deviceinfo.MainActivity.RAM;

import android.app.ActivityManager;
import android.content.Context;

import com.oaze.emad.deviceinfo.Utiles.RamData;

import static android.content.Context.ACTIVITY_SERVICE;

/**
 * Created by emad on 6/6/2017.
 */

public class RamPresenter implements RamContract.Presenter {

    private RamContract.View mView;
    private Context mContext;

    public RamPresenter(RamContract.View mView, Context context) {
        mContext = context;
        this.mView = mView;
    }

    @Override
    public void Start() {
        RamData ramData = getRAMData();
        mView.showUsedRam(String.valueOf(ramData.getUsed()));
        mView.showFreeRam(String.valueOf(ramData.getFree()));
        mView.showTotalRam(String.valueOf(ramData.getTotal()));
        mView.showProgressBar(ramData.getPercentage());
        mView.showPercentage(String.valueOf(100-ramData.getPercentage()));
    }


    public RamData getRAMData() {

        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ActivityManager activityManager = (ActivityManager) mContext.getSystemService(ACTIVITY_SERVICE);
        activityManager.getMemoryInfo(memoryInfo);

        float freeMem = (float) ((memoryInfo.availMem / 1024.0) / 1024.0);
        float totalMem = (float) ((memoryInfo.totalMem / 1024.0) / 1024.0);
        float usedMem = totalMem - freeMem;

        int percentage = (int) ((freeMem / totalMem) * 100);

        return new RamData(totalMem, usedMem, freeMem, percentage);
    }
}
