package com.oaze.emad.deviceinfo.MainActivity.OS;

import com.oaze.emad.deviceinfo.Utiles.SystemData;

import java.util.ArrayList;

/**
 * Created by emad on 6/6/2017.
 */

public interface OSContract {
    /**
     * Created by emad on 6/6/2017.
     */

    interface Presenter {
        Void CollectOSData();
        void Start();
    }

    /**
     * Created by emad on 6/6/2017.
     */

    interface View {
        void initializeRecycleView(ArrayList<SystemData> systemData);
    }
}
