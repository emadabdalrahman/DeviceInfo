package com.oaze.emad.deviceinfo.MainActivity.Device;

import com.oaze.emad.deviceinfo.Utiles.SystemData;

import java.util.ArrayList;

/**
 * Created by emad on 6/7/2017.
 */

public interface DeviceContract {

    public interface View {
        void initializeRecyclerView(ArrayList<SystemData> systemData);
    }

    public interface Presenter {
        void Start();
    }
}
