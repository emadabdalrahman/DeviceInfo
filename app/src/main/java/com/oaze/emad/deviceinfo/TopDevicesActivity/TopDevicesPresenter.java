package com.oaze.emad.deviceinfo.TopDevicesActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.oaze.emad.deviceinfo.Model.Remote.TopDevicesAsyncLoader;
import com.oaze.emad.deviceinfo.R;
import com.oaze.emad.deviceinfo.TopDevicesActivity.TopDeviceContract;
import com.oaze.emad.deviceinfo.Utiles.GSMTopDevices;
import com.oaze.emad.deviceinfo.Utiles.Network;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by emad on 6/11/2017.
 */

public class TopDevicesPresenter implements TopDeviceContract.Presenter, LoaderManager.LoaderCallbacks<Document> {


    public FragmentActivity mContext;
    private TopDeviceContract.View mView;
    private static ArrayList<GSMTopDevices> sGSMTopDevices = null;

    public TopDevicesPresenter(FragmentActivity context, TopDeviceContract.View view) {
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void Start() {

        mView.initializeLoadingView();
        mView.startLoadingView();
        if (sGSMTopDevices == null) {

            if (Network.isNetworkAvailable(mContext)) {
                initializeLoader();
            } else {
                sGSMTopDevices = getTopDevicesLocal();
                if (sGSMTopDevices == null) {
                    mView.showNoInternetConnectionGIF();
                    mView.playNoInternetConnectionGIF();
                    mView.hideLoadingView();
                    mView.showNoInternetConnection();
                    mView.showSnackbar();
                } else {
                    mView.hideNoInternetConnection();
                    mView.hideLoadingView();
                    mView.initializeRecyclerView(sGSMTopDevices);
                    mView.showSnackbar();
                }
            }

        } else {
            mView.hideNoInternetConnection();
            mView.hideLoadingView();
            mView.initializeRecyclerView(sGSMTopDevices);
            if (!Network.isNetworkAvailable(mContext)) {
                mView.showSnackbar();
            }

        }

    }

    @Override
    public ArrayList<GSMTopDevices> getGSMTopDevices(Document document) {
        ArrayList<GSMTopDevices> Devices = new ArrayList<>();

        Elements devicesTable = document.getElementsByClass("module-fit green");
        Element devicesTableBody = devicesTable.get(0).child(2);
        Elements devices = devicesTableBody.getElementsByTag("tr");
        for (int i = 1; i < devices.size(); i++) {
            String deviceName = devices.get(i).child(1).child(0).text();
            String deviceURL = devices.get(i).child(1).child(0).absUrl("href");
            Devices.add(new GSMTopDevices(deviceName, deviceURL));
        }
        return Devices;
    }

    @Override
    public void initializeLoader() {
        LoaderManager loaderManager = mContext.getSupportLoaderManager();
        Loader<Document> loader = loaderManager.getLoader(100);
        if (loader == null) {
            loaderManager.initLoader(100, null, this).forceLoad();
        } else {
            loaderManager.restartLoader(100, null, this).forceLoad();
        }
    }

    @Override
    public GSMTopDevices getDevice(int position) {
        if (sGSMTopDevices != null) {
            return sGSMTopDevices.get(position);
        }
        return null;
    }

    @Override
    public void saveTopDevicesLocal(ArrayList<GSMTopDevices> topDevices) {

        SharedPreferences sharedPreferences = mContext.getSharedPreferences("TopDevices", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        Set<String> devicesNames = new HashSet<>();
        Set<String> devicesURL = new HashSet<>();

        for (int i = 0; i < topDevices.size(); i++) {
            devicesNames.add(topDevices.get(i).getName());
            devicesURL.add(topDevices.get(i).getURL());
        }

        editor.putStringSet(mContext.getString(R.string.devicesNames), devicesNames);
        editor.putStringSet(mContext.getString(R.string.devicesURLs), devicesURL);

        editor.apply();

    }

    @Override
    public ArrayList<GSMTopDevices> getTopDevicesLocal() {
        ArrayList<GSMTopDevices> topDevices = new ArrayList<>();
        SharedPreferences sharedPreferences = mContext.getSharedPreferences("TopDevices", Context.MODE_PRIVATE);

        Set<String> devicesNames = sharedPreferences.getStringSet(mContext.getString(R.string.devicesNames), null);
        Set<String> devicesURLs = sharedPreferences.getStringSet(mContext.getString(R.string.devicesURLs), null);

        if (devicesNames != null && devicesURLs != null) {

            Iterator<String> URLs = sharedPreferences.getStringSet(mContext.getString(R.string.devicesURLs), null).iterator();
            Iterator<String> Names = sharedPreferences.getStringSet(mContext.getString(R.string.devicesNames), null).iterator();

            while (Names.hasNext()) {
                topDevices.add(new GSMTopDevices(Names.next(), URLs.next()));
            }

            return topDevices;

        }

        return null;
    }

    @Override
    public Loader<Document> onCreateLoader(int id, Bundle args) {
        return new TopDevicesAsyncLoader(mContext);
    }

    @Override
    public void onLoadFinished(Loader<Document> loader, Document data) {
        if (data != null) {
            sGSMTopDevices = getGSMTopDevices(data);
            saveTopDevicesLocal(sGSMTopDevices);
            mView.hideNoInternetConnection();
            mView.hideLoadingView();
            mView.initializeRecyclerView(sGSMTopDevices);
        } else {
            ArrayList<GSMTopDevices> topDevices = getTopDevicesLocal();
            if (topDevices == null){
                mView.hideLoadingView();
                mView.showNoInternetConnectionGIF();
                mView.playNoInternetConnectionGIF();
                mView.showNoInternetConnection();
            }else {
                sGSMTopDevices = topDevices;
                mView.hideNoInternetConnection();
                mView.hideLoadingView();
                mView.initializeRecyclerView(sGSMTopDevices);
                mView.showSnackbar();
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Document> loader) {

    }
}
