package com.oaze.emad.deviceinfo.MainActivity.Battery;

import android.content.Context;

/**
 * Created by emad on 6/7/2017.
 */

public class BatteryPresenter implements BatteryContract.Presenter {
    private BatteryContract.View mView;
    private Context mContext;

    public BatteryPresenter(BatteryContract.View View, Context Context) {
        this.mView = View;
        this.mContext = Context;
    }

    @Override
    public void Start() {

    }
}
