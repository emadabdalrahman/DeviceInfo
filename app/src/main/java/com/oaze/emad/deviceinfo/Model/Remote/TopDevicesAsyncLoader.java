package com.oaze.emad.deviceinfo.Model.Remote;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.oaze.emad.deviceinfo.R;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * Created by emad on 6/9/2017.
 */

public class TopDevicesAsyncLoader extends AsyncTaskLoader<Document> {

    private Context mContext;
    public TopDevicesAsyncLoader(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    public Document loadInBackground() {

        try {
            Connection.Response response = Jsoup.connect(mContext.getString(R.string.www_gsmarena_com))
                    .timeout(10*1000)
                    .userAgent(mContext.getString(R.string.UserAgent))
                    .execute();
            return response.parse();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
