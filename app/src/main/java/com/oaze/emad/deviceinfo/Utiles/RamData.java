package com.oaze.emad.deviceinfo.Utiles;

/**
 * Created by emad on 6/7/2017.
 */

public class RamData {
    private float Total;
    private float Used;
    private float Free;
    private int Percentage;

    public RamData(float total, float used, float free, int present) {
        Total = total;
        Used = used;
        Free = free;
        Percentage = present;
    }

    public RamData() {
    }

    public float getTotal() {
        return Total;
    }

    public void setTotal(float total) {
        Total = total;
    }

    public float getUsed() {
        return Used;
    }

    public void setUsed(float used) {
        Used = used;
    }

    public float getFree() {
        return Free;
    }

    public void setFree(float free) {
        Free = free;
    }

    public int getPercentage() {
        return Percentage;
    }

    public void setPercentage(int percentage) {
        Percentage = percentage;
    }
}
