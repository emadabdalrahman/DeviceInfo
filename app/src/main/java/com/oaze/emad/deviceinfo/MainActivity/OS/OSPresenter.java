package com.oaze.emad.deviceinfo.MainActivity.OS;

import android.content.Context;
import android.os.Build;

import com.oaze.emad.deviceinfo.R;
import com.oaze.emad.deviceinfo.Utiles.SystemData;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by emad on 6/6/2017.
 */

public class OSPresenter implements OSContract.Presenter {

    private OSContract.View mOSView;
    private Context mContext;

    public OSPresenter(OSContract.View mOSView, Context context) {
        this.mOSView = mOSView;
        mContext = context;
    }

    public ArrayList<SystemData> getOSData() {

        ArrayList<SystemData> systemData = new ArrayList<>();

        systemData.add(new SystemData(mContext.getString(R.string.FINGERPRINT), Build.FINGERPRINT));
        Date date = new Date(Build.TIME);
        systemData.add(new SystemData(mContext.getString(R.string.TIME), date.toString()));
        systemData.add(new SystemData(mContext.getString(R.string.ID), Build.ID));
        systemData.add(new SystemData(mContext.getString(R.string.SDK_INT), Build.VERSION.SDK_INT + ""));
        systemData.add(new SystemData(mContext.getString(R.string.SDK), Build.VERSION.SDK));
        systemData.add(new SystemData(mContext.getString(R.string.RELEASE), Build.VERSION.RELEASE));

        return systemData;
    }

    @Override
    public Void CollectOSData() {
        return null;
    }

    @Override
    public void Start() {
        mOSView.initializeRecycleView(getOSData());
    }

}
