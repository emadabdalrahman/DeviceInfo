package com.oaze.emad.deviceinfo.TopDevicesActivity;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oaze.emad.deviceinfo.R;
import com.oaze.emad.deviceinfo.Utiles.GSMTopDevices;

import java.util.ArrayList;

/**
 * Created by emad on 5/31/2017.
 */

public class TopDevicesRecyclerViewAdepter extends RecyclerView.Adapter<TopDevicesRecyclerViewAdepter.ViewHolder> {

    private ArrayList<GSMTopDevices> mDevices;
    private Context mContext;
    private ClickListener mClickListener;

    public interface ClickListener{
        void itemOnClickListener(int position);
    }

    public TopDevicesRecyclerViewAdepter(ArrayList<GSMTopDevices> Devices, Context context,ClickListener clickListener) {
        mDevices = Devices;
        mContext = context;
        mClickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.device_item,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
       holder.Name.setText(mDevices.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mDevices.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView Name;

        public ViewHolder(View itemView) {
            super(itemView);
            Name = (TextView)itemView.findViewById(R.id.device);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mClickListener.itemOnClickListener(getLayoutPosition());
                }
            });

        }
    }
}
