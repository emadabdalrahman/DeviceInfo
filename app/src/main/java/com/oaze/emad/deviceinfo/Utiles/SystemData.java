package com.oaze.emad.deviceinfo.Utiles;

/**
 * Created by emad on 6/6/2017.
 */

public class SystemData {
    private String Name;
    private String Data;

    public SystemData(String name, String data) {
        Name = name;
        Data = data;
    }

    public SystemData() {
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }
}
