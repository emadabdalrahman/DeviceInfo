package com.oaze.emad.deviceinfo.Utiles;

import org.jsoup.nodes.Document;

/**
 * Created by emad on 6/9/2017.
 */

public class GSMDeviceData {
    private String Name;
    private String ImageURL;
    private String ReleasedDate;
    private String Weight;
    private String LocalStorage;
    private String AndroidVersion;
    private String DisplaySize;
    private String DisplayResolution;
    private String Ram;
    private String Chipset;
    private String BatteryCapacity;
    private String BatteryTechnology;
    private String CameraPhoto;
    private String CameraVideo;
    private String Document;

    public String getDocument() {
        return Document;
    }

    public void setDocument(String document) {
        Document = document;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImageURL() {
        return ImageURL;
    }

    public void setImageURL(String imageURL) {
        ImageURL = imageURL;
    }

    public String getReleasedDate() {
        return ReleasedDate;
    }

    public void setReleasedDate(String releasedDate) {
        ReleasedDate = releasedDate;
    }

    public String getWeight() {
        return Weight;
    }

    public void setWeight(String weight) {
        Weight = weight;
    }

    public String getLocalStorage() {
        return LocalStorage;
    }

    public void setLocalStorage(String localStorage) {
        LocalStorage = localStorage;
    }

    public String getAndroidVersion() {
        return AndroidVersion;
    }

    public void setAndroidVersion(String androidVersion) {
        AndroidVersion = androidVersion;
    }

    public String getDisplaySize() {
        return DisplaySize;
    }

    public void setDisplaySize(String displaySize) {
        DisplaySize = displaySize;
    }

    public String getDisplayResolution() {
        return DisplayResolution;
    }

    public void setDisplayResolution(String displayResolution) {
        DisplayResolution = displayResolution;
    }

    public String getRam() {
        return Ram;
    }

    public void setRam(String ram) {
        Ram = ram;
    }

    public String getChipset() {
        return Chipset;
    }

    public void setChipset(String chipset) {
        Chipset = chipset;
    }

    public String getBatteryCapacity() {
        return BatteryCapacity;
    }

    public void setBatteryCapacity(String batteryCapacity) {
        BatteryCapacity = batteryCapacity;
    }

    public String getBatteryTechnology() {
        return BatteryTechnology;
    }

    public void setBatteryTechnology(String batteryTechnology) {
        BatteryTechnology = batteryTechnology;
    }

    public String getCameraPhoto() {
        return CameraPhoto;
    }

    public void setCameraPhoto(String cameraPhoto) {
        CameraPhoto = cameraPhoto;
    }

    public String getCameraVideo() {
        return CameraVideo;
    }

    public void setCameraVideo(String cameraVideo) {
        CameraVideo = cameraVideo;
    }
}
