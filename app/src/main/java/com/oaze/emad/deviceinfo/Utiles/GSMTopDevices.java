package com.oaze.emad.deviceinfo.Utiles;

/**
 * Created by emad on 6/21/2017.
 */

public class GSMTopDevices {
    private String Name;
    private String URL;

    public GSMTopDevices() {
    }

    public GSMTopDevices(String name, String URL) {
        Name = name;
        this.URL = URL;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String URL) {
        this.URL = URL;
    }
}
