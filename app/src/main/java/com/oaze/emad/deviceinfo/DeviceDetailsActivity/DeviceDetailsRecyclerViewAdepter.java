package com.oaze.emad.deviceinfo.DeviceDetailsActivity;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.oaze.emad.deviceinfo.R;
import com.oaze.emad.deviceinfo.Utiles.GSMDeviceData;
import com.oaze.emad.deviceinfo.Utiles.GSMTopDevices;

import java.util.ArrayList;

/**
 * Created by emad on 5/31/2017.
 */

public class DeviceDetailsRecyclerViewAdepter extends RecyclerView.Adapter<DeviceDetailsRecyclerViewAdepter.ViewHolder> {

    private GSMDeviceData mDeviceData;
    private Context mContext;
    private ArrayList<String> data = new ArrayList<>();



    public DeviceDetailsRecyclerViewAdepter(GSMDeviceData deviceData, Context context) {
        mDeviceData = deviceData;
        mContext = context;

        data.add(mDeviceData.getName());
        data.add(mDeviceData.getReleasedDate());
        data.add(mDeviceData.getLocalStorage());
        data.add(mDeviceData.getAndroidVersion());
        data.add(mDeviceData.getBatteryCapacity());
        data.add(mDeviceData.getBatteryTechnology());
        data.add(mDeviceData.getCameraPhoto());
        data.add(mDeviceData.getCameraVideo());
        data.add(mDeviceData.getDisplayResolution());
        data.add(mDeviceData.getChipset());
        data.add(mDeviceData.getRam());
        data.add(mDeviceData.getWeight());

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.device_item,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
       holder.Name.setText(data.get(position));
    }

    @Override
    public int getItemCount() {
        return 12;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView Name;

        public ViewHolder(View itemView) {
            super(itemView);
            Name = (TextView)itemView.findViewById(R.id.device);
        }
    }
}
