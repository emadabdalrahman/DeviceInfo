package com.oaze.emad.deviceinfo.MainActivity.Device;

import android.content.Context;
import android.os.Build;

import com.oaze.emad.deviceinfo.R;
import com.oaze.emad.deviceinfo.Utiles.SystemData;

import java.util.ArrayList;

/**
 * Created by emad on 6/7/2017.
 */

public class DevicePresenter implements DeviceContract.Presenter{

    private DeviceContract.View mView;
    private Context mContext;

    public DevicePresenter(DeviceContract.View mView, Context mContext) {
        this.mView = mView;
        this.mContext = mContext;
    }

    public ArrayList<SystemData> getOSData() {

        ArrayList<SystemData> systemData = new ArrayList<>();

        systemData.add(new SystemData(mContext.getString(R.string.CPU_ABI), Build.CPU_ABI));
        systemData.add(new SystemData(mContext.getString(R.string.CPU_ABI2), Build.CPU_ABI2));
        systemData.add(new SystemData(mContext.getString(R.string.BOARD), Build.BOARD));
        systemData.add(new SystemData(mContext.getString(R.string.BOOTLOADER), Build.BOOTLOADER));
        systemData.add(new SystemData(mContext.getString(R.string.BRAND), Build.BRAND));
        systemData.add(new SystemData(mContext.getString(R.string.DEVICE), Build.DEVICE));
        systemData.add(new SystemData(mContext.getString(R.string.DISPLAY), Build.DISPLAY));
        systemData.add(new SystemData(mContext.getString(R.string.HARDWARE), Build.HARDWARE));
        systemData.add(new SystemData(mContext.getString(R.string.HOST), Build.HOST));
        systemData.add(new SystemData(mContext.getString(R.string.MANUFACTURER), Build.MANUFACTURER));
        systemData.add(new SystemData(mContext.getString(R.string.MODEL), Build.MODEL));
        systemData.add(new SystemData(mContext.getString(R.string.PRODUCT), Build.PRODUCT));
        systemData.add(new SystemData(mContext.getString(R.string.SERIAL), Build.SERIAL));
        systemData.add(new SystemData(mContext.getString(R.string.TAGS), Build.TAGS));
        systemData.add(new SystemData(mContext.getString(R.string.USER), Build.USER));
        systemData.add(new SystemData(mContext.getString(R.string.TYPE), Build.TYPE));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            for (int i = 0; i < Build.SUPPORTED_ABIS.length; i++) {
                systemData.add(new SystemData(mContext.getString(R.string.SUPPORTED_ABIS), Build.SUPPORTED_ABIS[i]));
            }
            for (int i = 0; i < Build.SUPPORTED_32_BIT_ABIS.length; i++) {
                systemData.add(new SystemData(mContext.getString(R.string.SUPPORTED_32_BIT_ABIS), Build.SUPPORTED_32_BIT_ABIS[i]));
            }
            for (int i = 0; i < Build.SUPPORTED_64_BIT_ABIS.length; i++) {
                systemData.add(new SystemData(mContext.getString(R.string.SUPPORTED_64_BIT_ABIS), Build.SUPPORTED_64_BIT_ABIS[i]));
            }
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            systemData.add(new SystemData(mContext.getString(R.string.BASE_OS), Build.VERSION.BASE_OS));
        }
        systemData.add(new SystemData(mContext.getString(R.string.CODENAME), Build.VERSION.CODENAME));
        systemData.add(new SystemData(mContext.getString(R.string.INCREMENTAL), Build.VERSION.INCREMENTAL));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            systemData.add(new SystemData(mContext.getString(R.string.SECURITY_PATCH), Build.VERSION.SECURITY_PATCH));
        }
        return systemData;
    }

    @Override
    public void Start() {
        mView.initializeRecyclerView(getOSData());
    }
}
