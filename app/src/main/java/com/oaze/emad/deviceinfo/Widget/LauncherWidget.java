package com.oaze.emad.deviceinfo.Widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import com.oaze.emad.deviceinfo.MainActivity.MainActivity;
import com.oaze.emad.deviceinfo.R;

/**
 * Created by emad on 6/22/2017.
 */

public class LauncherWidget extends AppWidgetProvider {
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int i = 0; i < appWidgetIds.length; i++) {

            Intent intent = new Intent(context, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.launcher_app_widget);
            views.setOnClickPendingIntent(R.id.battery_widget_container, pendingIntent);

            appWidgetManager.updateAppWidget(appWidgetIds[i], views);

        }
    }
}
