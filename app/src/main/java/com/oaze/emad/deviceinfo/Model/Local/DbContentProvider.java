package com.oaze.emad.deviceinfo.Model.Local;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by emad on 6/9/2017.
 */

public class DbContentProvider extends ContentProvider{

    public static DbHelper dbHelper = null;
    public static final int DEVICE = 100;
    public static final UriMatcher sUriMatcher = buildUriMatcher();

    public static UriMatcher buildUriMatcher(){
        UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(DbContract.AUTHORITY,DbContract.PATH_DEVICE,DEVICE);
        return uriMatcher;
    };

    public DbHelper getDbHelper(){
      if (dbHelper == null){
          dbHelper = new DbHelper(getContext());
      }
      return dbHelper;
    }

    @Override
    public boolean onCreate() {
        dbHelper = getDbHelper();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteDatabase sqLiteDatabase = dbHelper.getReadableDatabase();
        Cursor returnCursor = null;
        switch (sUriMatcher.match(uri)){
            case DEVICE:
                returnCursor = sqLiteDatabase.query(DbContract.Device.TABLE_NAME,DbContract.Device.ALL_COLUMNS,null,null,null,null,null);
                break;
            default:
                break;
        }
        return returnCursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        SQLiteDatabase sqLiteDatabase = dbHelper.getWritableDatabase();
        Uri returnUri = null;
        switch (sUriMatcher.match(uri)){
            case DEVICE :
                long id = sqLiteDatabase.insert(DbContract.Device.TABLE_NAME,null,values);
                returnUri = ContentUris.withAppendedId(DbContract.Device.CONTENT_URI,id);
                break;
            default:
                returnUri = null;
                break;
        }
        return returnUri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
