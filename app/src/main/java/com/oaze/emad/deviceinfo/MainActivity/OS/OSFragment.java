package com.oaze.emad.deviceinfo.MainActivity.OS;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oaze.emad.deviceinfo.R;
import com.oaze.emad.deviceinfo.Utiles.SystemData;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by emad on 6/4/2017.
 */

public class OSFragment extends Fragment implements OSContract.View {

    private OSPresenter mOsPresenter;
    private FragmentActivity mContext;
    private View mRootView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = (FragmentActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.fragment, container, false);
        mOsPresenter = new OSPresenter(this,mContext);
        mOsPresenter.Start();

        return mRootView;
    }

    @Override
    public void initializeRecycleView(ArrayList<SystemData> systemData) {

        RecyclerView recyclerView = (RecyclerView) mRootView.findViewById(R.id.fragmentRecyclerView);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(layoutManager);

        recyclerViewAdepter adepter = new recyclerViewAdepter(systemData, mContext);
        recyclerView.setAdapter(adepter);

    }

}
