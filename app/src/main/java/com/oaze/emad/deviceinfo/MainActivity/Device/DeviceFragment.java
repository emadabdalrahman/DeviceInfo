package com.oaze.emad.deviceinfo.MainActivity.Device;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oaze.emad.deviceinfo.R;
import com.oaze.emad.deviceinfo.Utiles.SystemData;

import java.util.ArrayList;

/**
 * Created by emad on 6/6/2017.
 */

public class DeviceFragment extends Fragment implements DeviceContract.View{

    private FragmentActivity mContext;
    private View mRootView;
    private DevicePresenter mDevicePresenter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = (FragmentActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment,container,false);

        mDevicePresenter = new DevicePresenter(this,mContext);
        mDevicePresenter.Start();

        return mRootView;
    }

    @Override
    public void initializeRecyclerView(ArrayList<SystemData> systemData) {

        RecyclerView recyclerView = (RecyclerView) mRootView.findViewById(R.id.fragmentRecyclerView);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(layoutManager);

        DeviceRecyclerViewAdepter adepter = new DeviceRecyclerViewAdepter(systemData, mContext);
        recyclerView.setAdapter(adepter);

    }
}
