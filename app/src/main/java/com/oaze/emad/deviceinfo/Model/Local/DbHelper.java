package com.oaze.emad.deviceinfo.Model.Local;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by emad on 6/9/2017.
 */

public class DbHelper extends SQLiteOpenHelper {


    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Device.db";
    public static final String SQL_CREATE_DEVICE_TABLE =
            "CREATE TABLE " + DbContract.Device.TABLE_NAME + " ("
                    + DbContract.Device.DEVICE_NAME + " TEXT,"
                    + DbContract.Device.DEVICE_DATA + " TEXT" + ")";


    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_DEVICE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_CREATE_DEVICE_TABLE);
        onCreate(db);
    }
}
