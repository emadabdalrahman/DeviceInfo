package com.oaze.emad.deviceinfo.DeviceDetailsActivity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.cunoraz.gifview.library.GifView;
import com.ldoublem.loadingviewlib.view.LVGhost;
import com.oaze.emad.deviceinfo.R;
import com.oaze.emad.deviceinfo.Utiles.GSMDeviceData;
import com.squareup.picasso.Picasso;

/**
 * Created by emad on 6/9/2017.
 */

public class DeviceDetailsFragment extends Fragment implements DeviceDetailsContract.View {

    private FragmentActivity mContext;
    private View mRootView;
    private DeviceDetailsPresenter mDeviceDetailsPresenter;
    private GifView mGifView;
    private RelativeLayout mOfflineLayout;
    private LVGhost mLvGhostLoadingView;
    private RelativeLayout mHeader;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = (FragmentActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mRootView = inflater.inflate(R.layout.device_details_fragment, container, false);
        mOfflineLayout = (RelativeLayout) mRootView.findViewById(R.id.device_details_fragment_offline_layout);
        mHeader = (RelativeLayout)mRootView.findViewById(R.id.device_details_fragment_hearder);

        mDeviceDetailsPresenter = new DeviceDetailsPresenter(mContext, this);
        mDeviceDetailsPresenter.start();

        return mRootView;
    }


    @Override
    public void playNoInternetConnectionGIF() {
        mGifView.play();
    }

    @Override
    public void pauseNoInternetConnectionGIF() {
        mGifView.pause();
    }

    @Override
    public void showNoInternetConnectionGIF() {
        mGifView = (GifView) mRootView.findViewById(R.id.device_details_fragment_offline_gif);
        mGifView.setVisibility(View.VISIBLE);
        mGifView.setGifResource(R.drawable.no_internet_connection);
    }

    @Override
    public boolean isNoInternetConnectionVisible() {
        return mOfflineLayout.isShown();
    }

    @Override
    public void hideNoInternetConnection() {
        if (isNoInternetConnectionVisible()) {
            mOfflineLayout.setVisibility(View.INVISIBLE);
          //  mRootView.setBackgroundResource(R.color.colorPrimary);
            mHeader.setVisibility(View.VISIBLE);

            showNoInternetConnection();
        }
    }

    @Override
    public void showNoInternetConnection() {
        if (!isNoInternetConnectionVisible()) {
            mOfflineLayout.setVisibility(View.VISIBLE);
            //m.setBackgroundResource(R.color.white2);
        }
    }

    @Override
    public void initializeLoadingView() {
        mLvGhostLoadingView = (LVGhost) mRootView.findViewById(R.id.device_details_fragment_loadingView);
        mLvGhostLoadingView.setViewColor(Color.WHITE);
        mLvGhostLoadingView.setHandColor(Color.BLACK);
    }

    @Override
    public void startLoadingView() {
        mLvGhostLoadingView.startAnim();
    }

    @Override
    public void stopLoadingView() {
        mLvGhostLoadingView.stopAnim();
    }

    @Override
    public void hideLoadingView() {
        mLvGhostLoadingView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showDeviceImage(String URL) {
        ImageView imageView = (ImageView)mRootView.findViewById(R.id.device_details_fragment_hearder_image);
        mHeader.setVisibility(View.VISIBLE);
        Picasso.with(mContext).load(URL).into(imageView);
    }

    @Override
    public void initializeRecyclerView(GSMDeviceData deviceData) {
        RecyclerView recyclerView = (RecyclerView) mRootView.findViewById(R.id.device_details_fragment_recyclerView);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(linearLayoutManager);

        DeviceDetailsRecyclerViewAdepter adepter = new DeviceDetailsRecyclerViewAdepter(deviceData, mContext);
        recyclerView.setAdapter(adepter);
    }

}
