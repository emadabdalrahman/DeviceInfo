package com.oaze.emad.deviceinfo.DeviceDetailsActivity;

import com.oaze.emad.deviceinfo.Utiles.GSMDeviceData;

import org.jsoup.nodes.Document;

/**
 * Created by emad on 6/27/2017.
 */

public interface DeviceDetailsContract {
    public interface View {
        void playNoInternetConnectionGIF();
        void pauseNoInternetConnectionGIF();
        void showNoInternetConnectionGIF();
        boolean isNoInternetConnectionVisible();
        void hideNoInternetConnection();
        void showNoInternetConnection();
        void initializeLoadingView();
        void startLoadingView();
        void stopLoadingView();
        void hideLoadingView();
        void showDeviceImage(String URL);
        void initializeRecyclerView(GSMDeviceData deviceData);
    }

    public interface Presenter {

        void start();
        void restart();
        void initializeLoader();
        boolean saveDeviceLocal(GSMDeviceData deviceData);
        boolean isSavedLocal(String deviceName);
        GSMDeviceData getDeviceLocal(String deviceName);
        GSMDeviceData getGSMDeviceData(Document document);
    }
}
