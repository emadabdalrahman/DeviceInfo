package com.oaze.emad.deviceinfo.Model.Local;

import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by emad on 6/9/2017.
 */

public class DbContract {

    public static final String AUTHORITY = "com.oaze.emad.deviceinfo" ;
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://"+AUTHORITY);
    public static final String PATH_DEVICE = "Device";

    public static class Device implements BaseColumns{

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_DEVICE).build();

        public static final String[] ALL_COLUMNS = {
                Device.DEVICE_NAME,
                Device.DEVICE_DATA
        };

        public static final String TABLE_NAME = "Device";
        public static final String DEVICE_NAME = "Name";
        public static final String DEVICE_DATA = "Data";
    }
}
