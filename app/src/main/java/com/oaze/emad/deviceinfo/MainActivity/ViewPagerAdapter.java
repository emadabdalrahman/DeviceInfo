package com.oaze.emad.deviceinfo.MainActivity;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.oaze.emad.deviceinfo.MainActivity.Battery.BatteryFragment;
import com.oaze.emad.deviceinfo.MainActivity.Device.DeviceFragment;
import com.oaze.emad.deviceinfo.MainActivity.OS.OSFragment;
import com.oaze.emad.deviceinfo.MainActivity.RAM.RamFragment;
import com.oaze.emad.deviceinfo.MainActivity.Sensors.SensorsFragment;
import com.oaze.emad.deviceinfo.R;
import com.oaze.emad.deviceinfo.TopDevicesActivity.TopDevicesFragment;

import java.util.ArrayList;

/**
 * Created by emad on 6/4/2017.
 */

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<String> mTitles = new ArrayList<>();

    Fragment[] fragments ={
            new SensorsFragment(),
            new DeviceFragment(),
            new OSFragment(),
            new RamFragment(),
            new BatteryFragment()
    };

    public ViewPagerAdapter(FragmentManager fm,ArrayList<String> titles) {
        super(fm);
        mTitles = titles;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return mTitles.size();
    }
}
