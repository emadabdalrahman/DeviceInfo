package com.oaze.emad.deviceinfo.Utiles;

/**
 * Created by emad on 5/31/2017.
 */

public class SensorData {

    private String Name;
    private String StringType;
    private int Type;
    private String Vendor;
    private int Version;
    private float Power;
    private int ID;
    private float Resolution;
    private float MaximumRange;
    private boolean WakeUp;
    private boolean Dynamic;

    public boolean isWakeUp() {
        return WakeUp;
    }

    public void setWakeUp(boolean wakeUp) {
        WakeUp = wakeUp;
    }

    public boolean isDynamic() {
        return Dynamic;
    }

    public void setDynamic(boolean dynamic) {
        Dynamic = dynamic;
    }

    public String getStringType() {
        return StringType;
    }

    public void setStringType(String stringType) {
        StringType = stringType;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    public String getVendor() {
        return Vendor;
    }

    public void setVendor(String vendor) {
        Vendor = vendor;
    }

    public int getVersion() {
        return Version;
    }

    public void setVersion(int version) {
        Version = version;
    }

    public float getPower() {
        return Power;
    }

    public void setPower(float power) {
        Power = power;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public float getResolution() {
        return Resolution;
    }

    public void setResolution(float resolution) {
        Resolution = resolution;
    }

    public float getMaximumRange() {
        return MaximumRange;
    }

    public void setMaximumRange(float maximumRange) {
        MaximumRange = maximumRange;
    }
}
