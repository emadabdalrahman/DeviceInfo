package com.oaze.emad.deviceinfo.Utiles;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.jsoup.Connection;

/**
 * Created by emad on 6/21/2017.
 */

public class Network {

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean isResponseSuccess(Connection.Response response) {
        if (response.statusCode() == 200) {
            return true;
        } else {
            return false;
        }
    }

    public static String getResponseDescription(Connection.Response response) {
        if (response.statusCode() == 100) {
            return "The server has received the request headers, and the client should proceed to send the request body";
        } else if (response.statusCode() == 101) {
            return "The requester has asked the server to switch protocols";
        } else if (response.statusCode() == 103) {
            return "Used in the resumable requests proposal to resume aborted PUT or POST requests";
        } else if (response.statusCode() == 200) {
            return "The request is OK (this is the standard response for successful HTTP requests)";
        } else if (response.statusCode() == 201) {
            return "The request has been fulfilled, and a new resource is created ";
        } else if (response.statusCode() == 202) {
            return "The request has been accepted for processing, but the processing has not been completed";
        } else if (response.statusCode() == 203) {
            return "The request has been successfully processed, but is returning information that may be from another source";
        } else if (response.statusCode() == 204) {
            return "The request has been successfully processed, but is not returning any content";
        } else if (response.statusCode() == 205) {
            return "The request has been successfully processed, but is not returning any content, and requires that the requester reset the document view";
        } else if (response.statusCode() == 206) {
            return "The server is delivering only part of the resource due to a range header sent by the client";
        } else if (response.statusCode() == 300) {
            return "A link list. The user can select a link and go to that location. Maximum five addresses";
        } else if (response.statusCode() == 301) {
            return "The requested page has moved to a new URL";
        } else if (response.statusCode() == 302) {
            return "The requested page has moved temporarily to a new URL";
        } else if (response.statusCode() == 303) {
            return "The requested page can be found under a different URL";
        } else if (response.statusCode() == 304) {
            return "Indicates the requested page has not been modified since last requested";
        } else if (response.statusCode() == 306) {
            return "No longer used";
        } else if (response.statusCode() == 307) {
            return "The requested page has moved temporarily to a new URL";
        } else if (response.statusCode() == 308) {
            return "Used in the resumable requests proposal to resume aborted PUT or POST requests";
        } else if (response.statusCode() == 400) {
            return "The request cannot be fulfilled due to bad syntax";
        } else if (response.statusCode() == 401) {
            return "The request was a legal request, but the server is refusing to respond to it. For use when authentication is possible but has failed or not yet been provided";
        } else if (response.statusCode() == 402) {
            return "Reserved for future use";
        } else if (response.statusCode() == 403) {
            return "The request was a legal request, but the server is refusing to respond to it";
        } else if (response.statusCode() == 404) {
            return "The requested page could not be found but may be available again in the future";
        } else if (response.statusCode() == 405) {
            return "A request was made of a page using a request method not supported by that page";
        } else if (response.statusCode() == 406) {
            return "The server can only generate a response that is not accepted by the client";
        } else if (response.statusCode() == 407) {
            return "The client must first authenticate itself with the proxy";
        } else if (response.statusCode() == 408) {
            return "The server timed out waiting for the request";
        } else if (response.statusCode() == 409) {
            return "The request could not be completed because of a conflict in the request";
        } else if (response.statusCode() == 410) {
            return "The requested page is no longer available";
        } else if (response.statusCode() == 411) {
            return "The \"Content-Length\" is not defined. The server will not accept the request without it";
        } else if (response.statusCode() == 412) {
            return "The precondition given in the request evaluated to false by the server";
        } else if (response.statusCode() == 413) {
            return "The server will not accept the request, because the request entity is too large";
        } else if (response.statusCode() == 414) {
            return "The server will not accept the request, because the URL is too long. Occurs when you convert a POST request to a GET request with a long query information";
        } else if (response.statusCode() == 415) {
            return "The server will not accept the request, because the media type is not supported";
        } else if (response.statusCode() == 416) {
            return "The client has asked for a portion of the file, but the server cannot supply that portion";
        } else if (response.statusCode() == 417) {
            return "The server cannot meet the requirements of the Expect request-header field";
        } else if (response.statusCode() == 500) {
            return "A generic error message, given when no more specific message is suitable";
        } else if (response.statusCode() == 501) {
            return "The server either does not recognize the request method, or it lacks the ability to fulfill the request";
        } else if (response.statusCode() == 502) {
            return "The server was acting as a gateway or proxy and received an invalid response from the upstream server";
        } else if (response.statusCode() == 503) {
            return "The server is currently unavailable (overloaded or down)";
        } else if (response.statusCode() == 504) {
            return "The server was acting as a gateway or proxy and did not receive a timely response from the upstream server";
        } else if (response.statusCode() == 505) {
            return "The server does not support the HTTP protocol version used in the request";
        } else if (response.statusCode() == 511) {
            return "The client needs to authenticate to gain network access";
        } else {
            return "NO Description";
        }
    }

}
