package com.oaze.emad.deviceinfo.MainActivity.Sensors;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oaze.emad.deviceinfo.R;
import com.oaze.emad.deviceinfo.Utiles.SensorData;

import java.util.ArrayList;

/**
 * Created by emad on 6/6/2017.
 */

public class SensorsFragment extends Fragment implements SensorsContract.View {

    private View mRootView;
    private FragmentActivity mContext;
    private SensorsPresenter mSensorsPresenter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = (FragmentActivity) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.fragment, container, false);

        mSensorsPresenter = new SensorsPresenter(this,mContext);
        mSensorsPresenter.Start();

        return mRootView;
    }

    @Override
    public void initializeRecyclerView(ArrayList<SensorData> sensorData) {
        RecyclerView recyclerView = (RecyclerView)mRootView.findViewById(R.id.fragmentRecyclerView);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(layoutManager);

        SensorsRecyclerViewAdepter adepter = new SensorsRecyclerViewAdepter(sensorData,mContext);
        recyclerView.setAdapter(adepter);
    }

}
